package com.atlassian.confluence.plugins.linkfixer;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.core.ContentEntityManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.util.longrunning.LongRunningTaskId;
import com.atlassian.confluence.util.longrunning.LongRunningTaskManager;
import com.atlassian.core.task.longrunning.LongRunningTask;
import com.atlassian.sal.api.transaction.TransactionTemplate;

/**
 * An action that allows the triggering of the conversion of links to a particular space to become relative.
 */
public class LinkFixAction extends ConfluenceActionSupport
{
    private SpaceManager spaceManager;
    private PageLinkQueryDao pageLinkQueryDao;
    private ResourceIdentifierModifier resourceIdentifierModifier;
    private ContentEntityManager contentEntityManager;
    private LongRunningTaskManager longRunningTaskManager;
    private TransactionTemplate transactionTemplate;
    
    private String spaceKey;
    private String destinationSpaceKey;
    
    private LongRunningTaskId taskId;
    private LongRunningTask task;
    
    // Action results
    private Space space;
    private Space destinationSpace;
    private List<String> exceptionsReport;
    
    
    @Override
    public void validate()
    {
        if (StringUtils.isBlank(spaceKey) || StringUtils.isBlank(destinationSpaceKey))
            addActionError("Both a space key and destination space key are required.");
        
        space = spaceManager.getSpace(spaceKey);
        if (space == null)
            addActionError("error.space.could.not.be.found", spaceKey);
            
        destinationSpace = spaceManager.getSpace(destinationSpaceKey);
        if (destinationSpace == null)
            addActionError("error.space.could.not.be.found", destinationSpace);
    }

    /**
     * Instigate the long running conversion process.
     * 
     * @return
     */
    public String doConversion()
    {
        task = new ConvertToRelativeLinksLongRunningTask(
                pageLinkQueryDao, space, destinationSpace,
                transactionTemplate, resourceIdentifierModifier,
                contentEntityManager, getRemoteUser()); 
        
        taskId = longRunningTaskManager.startLongRunningTask(getRemoteUser(), task);
        return SUCCESS;
    }
    
    public String doExceptionReport()
    {
        ConvertToRelativeLinksLongRunningTask longRunningTask = (ConvertToRelativeLinksLongRunningTask) getTask();
        if (longRunningTask == null)
        {
            exceptionsReport = Collections.emptyList();
        }
        else
        {
            List<Exception> exceptions = longRunningTask.getExceptions();
            exceptionsReport = new ArrayList<String>(exceptions.size());
            
            for (Exception ex: exceptions)
            {
                String exStr = ex.toString() + "<br><pre>";
                StringWriter exOutput = new StringWriter();
                PrintWriter writer = new PrintWriter(exOutput);
                ex.printStackTrace(writer);
                
                writer.flush();
                exStr = exStr + exOutput.getBuffer().toString() + "</pre>";
                exceptionsReport.add(exStr);
            }
        }
        
        return SUCCESS;
    }
    
    public void setPageLinkQueryDao(PageLinkQueryDao pageLinkQueryDao)
    {
        this.pageLinkQueryDao = pageLinkQueryDao;
    }
    
    public void setSpaceManager(SpaceManager spaceManager)
    {
        this.spaceManager = spaceManager;
    }

    public void setTransactionTemplate(TransactionTemplate transactionTemplate)
    {
        this.transactionTemplate = transactionTemplate;
    }

    public void setResourceIdentifierModifier(ResourceIdentifierModifier resourceIdentifierModifier)
    {
        this.resourceIdentifierModifier = resourceIdentifierModifier;
    }

    public void setContentEntityManager(ContentEntityManager contentEntityManager)
    {
        this.contentEntityManager = contentEntityManager;
    }

    public void setLongRunningTaskManager(LongRunningTaskManager longRunningTaskManager)
    {
        this.longRunningTaskManager = longRunningTaskManager;
    }

    public String getSpaceKey()
    {
        return spaceKey;
    }

    public void setSpaceKey(String spaceKey)
    {
        this.spaceKey = spaceKey;
    }

    public String getDestinationSpaceKey()
    {
        return destinationSpaceKey;
    }

    public void setDestinationSpaceKey(String destinationSpaceKey)
    {
        this.destinationSpaceKey = destinationSpaceKey;
    }

    public String getTaskId()
    {
        return taskId.toString();
    }
    
    public void setTaskId(String taskId)
    {
        this.taskId = LongRunningTaskId.valueOf(taskId);
    }

    public LongRunningTask getTask()
    {
        if (task == null && taskId != null)
        {
            return longRunningTaskManager.getLongRunningTask(getRemoteUser(), taskId);
        }
        else
        {
            return null;
        }
    }

    public List<String> getExceptionsReportHtml()
    {
        return exceptionsReport;
    }
}
