package com.atlassian.confluence.plugins.linkfixer.ownspace;

import java.util.Arrays;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.atlassian.confluence.plugins.linkfixer.PageIdentifyingReportItem;
import com.atlassian.util.concurrent.atomic.AtomicInteger;

public class AbsoluteLinkReport
{
    private Queue<PageIdentifyingReportItem> entries;
    private AtomicInteger processed;

    public AbsoluteLinkReport()
    {
        super();
        entries = new ConcurrentLinkedQueue<PageIdentifyingReportItem>();
        processed = new AtomicInteger(0);
    }
    
    public void add(PageIdentifyingReportItem item)
    {
        entries.add(item);
    }
    
    public void processed()
    {
        processed.incrementAndGet();
    }
    
    public int size()
    {
        return entries.size();
    }

    public List<PageIdentifyingReportItem> getEntries()
    {
        PageIdentifyingReportItem[] report = new PageIdentifyingReportItem[entries.size()];
        return Arrays.asList(entries.toArray(report));
    }    
    
    public int getProcessed()
    {
        return processed.get();
    }
}
