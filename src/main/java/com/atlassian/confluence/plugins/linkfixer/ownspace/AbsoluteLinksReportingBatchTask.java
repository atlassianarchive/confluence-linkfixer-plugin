package com.atlassian.confluence.plugins.linkfixer.ownspace;

import com.atlassian.confluence.content.render.xhtml.migration.BatchException;
import com.atlassian.confluence.content.render.xhtml.migration.BatchTask;
import com.atlassian.confluence.core.SpaceContentEntityObject;
import com.atlassian.confluence.plugins.linkfixer.PageIdentifyingReportItem;
import com.atlassian.confluence.plugins.linkfixer.ResourceIdentifierModifier;

/**
 * A BatchTask that will update a supplied report with a list of
 * pages which have absolute links to the same space.
 * 
 * 
 */
public class AbsoluteLinksReportingBatchTask implements BatchTask<SpaceContentEntityObject>
{
    private final String spaceKey;
    private final ResourceIdentifierModifier resourceIdentifierModifier;
    private final AbsoluteLinkReport report;
    
    public AbsoluteLinksReportingBatchTask(String spaceKey, ResourceIdentifierModifier resourceIdentifierModifier, AbsoluteLinkReport report)
    {
        this.spaceKey = spaceKey;
        this.resourceIdentifierModifier = resourceIdentifierModifier;
        this.report = report;
    }

    @Override
    public void apply(SpaceContentEntityObject item, int index, int batchSize) throws Exception, BatchException
    {
        String content = item.getBodyAsString();
        
        int count = resourceIdentifierModifier.countSpaceKeyReferences(content, spaceKey);
        if (count > 0)
            report.add(new PageIdentifyingReportItem(item.getId(), item.getTitle()));
        
        report.processed();
    }
}
