package com.atlassian.confluence.plugins.linkfixer;

import java.util.List;

import com.atlassian.confluence.core.SpaceContentEntityObject;
import com.atlassian.confluence.spaces.Space;

public interface PageLinkQueryDao
{

    public List<PageIdentifyingReportItem> getReportOfSpacedContentWhichLinkToSpace(Space space, Space destinationSpace);
    
    /**
     * Get a list of the SpaceContentEntityObject in a particular space which have links to another space.
     * 
     * @param space the space to retrieve pages from
     * @param destinationSpace the space which the pages should link to 
     * @param maxRows the maximum number of results to return
     * @return a List of SpaceContentEntityObject which link to the specified space
     */
    public List<SpaceContentEntityObject> getSpacedContentWhichLinkToSpace(Space space, Space destinationSpace, int maxRows);
    
    public List<SpaceContentEntityObject> getSpacedContentWhichLinkToSpace(Space space, Space destinationSpace, long startContentId, int maxRows);
    
    public int countSpacedContentWhichLinkToSpace(Space space, Space destinationSpace);
}