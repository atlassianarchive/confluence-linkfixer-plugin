package com.atlassian.confluence.plugins.linkfixer;

import static com.atlassian.confluence.content.render.xhtml.XhtmlConstants.RESOURCE_IDENTIFIER_NAMESPACE_PREFIX;
import static com.atlassian.confluence.content.render.xhtml.XhtmlConstants.RESOURCE_IDENTIFIER_NAMESPACE_URI;
import static com.atlassian.confluence.content.render.xhtml.storage.resource.identifiers.StorageResourceIdentifierConstants.SPACEKEY_ATTRIBUTE_NAME;
import static com.atlassian.confluence.content.render.xhtml.storage.resource.identifiers.StorageResourceIdentifierConstants.SPACE_RESOURCE_QNAME;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.apache.commons.lang.StringUtils;

import com.atlassian.confluence.content.render.xhtml.StaxUtils;
import com.atlassian.confluence.content.render.xhtml.XmlEventReaderFactory;
import com.atlassian.confluence.content.render.xhtml.XmlOutputFactoryFactoryBean;

public class ResourceIdentifierModifierImpl implements ResourceIdentifierModifier
{
    public static final QName SPACEKEY_ATTRIBUTE_QNAME = new QName(RESOURCE_IDENTIFIER_NAMESPACE_URI, SPACEKEY_ATTRIBUTE_NAME, RESOURCE_IDENTIFIER_NAMESPACE_PREFIX);
    
    private final XmlEventReaderFactory xmlEventReaderFactory;
    private final XMLOutputFactory xmlOutputFactory;
    private final XMLEventFactory xmlEventFactory;
    
    public ResourceIdentifierModifierImpl(XmlEventReaderFactory xmlEventReaderFactory) throws Exception
    {
        this.xmlEventReaderFactory = xmlEventReaderFactory;
        this.xmlOutputFactory = (XMLOutputFactory) (new XmlOutputFactoryFactoryBean(true)).getObject();
        this.xmlEventFactory = XMLEventFactory.newInstance();
    }


    @Override
    public ResourceIdentifierModifierResult removeSpaceKey(String content, String keyValue)
    {
        if (StringUtils.isBlank(content))
            return new ResourceIdentifierModifierResult("", 0);

        StringWriter stringWriter = new StringWriter();
        XMLEventReader xmlEventReader = null;
        XMLEventWriter xmlEventWriter = null;
        int modifications = 0;
        try
        {
            xmlEventReader = xmlEventReaderFactory.createStorageXmlEventReader(new StringReader(content));
            xmlEventWriter = xmlOutputFactory.createXMLEventWriter(stringWriter);

            while (xmlEventReader.hasNext())
            {
                XMLEvent currentEvent = xmlEventReader.nextEvent();

                if (currentEvent.isStartElement())
                {
                    StartElement element = currentEvent.asStartElement();
                    Attribute spaceKeyAttr = element.getAttributeByName(SPACEKEY_ATTRIBUTE_QNAME);
                    // space resource identifiers must retain their space-key attribute
                    if (!element.getName().equals(SPACE_RESOURCE_QNAME) && spaceKeyAttr != null && keyValue.equals(spaceKeyAttr.getValue()))
                    {
                        List<Attribute> attributesToPreserve = new ArrayList<Attribute>();
                        Iterator<Attribute> attributesIterator = (Iterator<Attribute>) element.getAttributes();
                        
                        while (attributesIterator.hasNext())
                        {
                            Attribute attribute = attributesIterator.next();
                            
                            if (!SPACEKEY_ATTRIBUTE_QNAME.equals(attribute.getName()))
                                attributesToPreserve.add(attribute);
                        }
                        
                        xmlEventWriter.add(xmlEventFactory.createStartElement(element.getName(), attributesToPreserve.iterator(), null));
                        modifications++;
                    }
                    else
                    {
                        xmlEventWriter.add(currentEvent);
                    }
                }
                else
                {
                    xmlEventWriter.add(currentEvent);
                }
            }
        }
        catch (XMLStreamException e)
        {
            throw new RuntimeException(e);
        }
        finally
        {
            StaxUtils.closeQuietly(xmlEventReader);
            StaxUtils.closeQuietly(xmlEventWriter);
        }
        
        if (modifications == 0)
        {
            return new ResourceIdentifierModifierResult(content, modifications);
        }
        else
        {
            return new ResourceIdentifierModifierResult(stringWriter.toString(), modifications);
        }
    }


    @Override
    public int countSpaceKeyReferences(String content, String key)
    {
        if (StringUtils.isBlank(content))
            return 0;

        XMLEventReader xmlEventReader = null;
        int count = 0;
        try
        {
            xmlEventReader = xmlEventReaderFactory.createStorageXmlEventReader(new StringReader(content));

            while (xmlEventReader.hasNext())
            {
                XMLEvent currentEvent = xmlEventReader.nextEvent();

                if (currentEvent.isStartElement())
                {
                    StartElement element = currentEvent.asStartElement();
                    Attribute spaceKeyAttr = element.getAttributeByName(SPACEKEY_ATTRIBUTE_QNAME);
                    // space resource identifiers must retain their space-key attribute so don't include in the count
                    if (!element.getName().equals(SPACE_RESOURCE_QNAME) && spaceKeyAttr != null && key.equals(spaceKeyAttr.getValue()))
                    {
                        count++;
                    }
                }
            }
        }
        catch (XMLStreamException e)
        {
            throw new RuntimeException(e);
        }
        finally
        {
            StaxUtils.closeQuietly(xmlEventReader);
        }

        return count;
    }
}
