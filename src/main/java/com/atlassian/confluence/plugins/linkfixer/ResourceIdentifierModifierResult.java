package com.atlassian.confluence.plugins.linkfixer;

public class ResourceIdentifierModifierResult
{
    private final String content;
    private final int modificationCount;
    
    public ResourceIdentifierModifierResult(String content, int modificationCount)
    {
        this.content = content;
        this.modificationCount = modificationCount;
    }

    public String getContent()
    {
        return content;
    }

    public int getModificationCount()
    {
        return modificationCount;
    }
    
    public boolean isModified()
    {
        return modificationCount > 0;
    }
}
