package com.atlassian.confluence.plugins.linkfixer;

public class PageIdentifyingReportItem
{
    private final long id;
    private final String title;
    
    public PageIdentifyingReportItem(long id, String title)
    {
        this.id = id;
        this.title = title;
    }
    
    public long getId()
    {
        return id;
    }

    public String getTitle()
    {
        return title;
    }

    @Override
    public boolean equals(Object o)
    {
        if (o == this)
            return true;
        
        if (!(o instanceof PageIdentifyingReportItem))
            return false;
        
        PageIdentifyingReportItem other = (PageIdentifyingReportItem)o;
        
        return other.id == id && other.title.equals(title);
    }

    @Override
    public int hashCode()
    {
        return 35 * (title.hashCode() + (int)id);
    }
}
