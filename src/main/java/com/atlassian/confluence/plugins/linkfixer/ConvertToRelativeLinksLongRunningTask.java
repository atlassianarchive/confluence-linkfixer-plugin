package com.atlassian.confluence.plugins.linkfixer;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import com.atlassian.confluence.content.render.xhtml.migration.BatchTask;
import com.atlassian.confluence.content.render.xhtml.migration.BatchableWorkSource;
import com.atlassian.confluence.core.ContentEntityManager;
import com.atlassian.confluence.core.SpaceContentEntityObject;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.util.longrunning.ConfluenceAbstractLongRunningTask;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.user.User;

public class ConvertToRelativeLinksLongRunningTask extends ConfluenceAbstractLongRunningTask
{
    private final BatchableWorkSource<SpaceContentEntityObject> workSource;    
    private final SALWorkSourceBatchRunner<SpaceContentEntityObject> runner;
    private final BatchTask<SpaceContentEntityObject> batchTask;
    
    // results from the task
    private List<Exception> exceptions;
    
    public ConvertToRelativeLinksLongRunningTask(PageLinkQueryDao pageLinkQueryDao, Space space, Space destinationSpace,
            TransactionTemplate transactionTemplate, ResourceIdentifierModifier resourceIdentifierModifier,
            ContentEntityManager contentEntityManager, User user)
    {
        super();
        this.workSource = new LinksInSpaceWorkSource(pageLinkQueryDao, space, destinationSpace, Integer.getInteger("upgrade.fix.batch.size", 100));
        this.runner = new SALWorkSourceBatchRunner<SpaceContentEntityObject>("Convert to relative links Fixing Thread", Integer.getInteger("upgrade.fix.threads", 4), transactionTemplate);
        this.runner.setProgressWrapper(progress);
        this.batchTask = new CreateRelativeLinksBatchTask(destinationSpace.getKey(), resourceIdentifierModifier, contentEntityManager, user);
        this.exceptions = new ArrayList<Exception>();
    }
    
    @Override
    public String getName()
    {
        return "Convert Links to Relative";
    }

    @Override
    protected void runInternal()
    {
        try
        {
            List<Exception> results = runner.run(workSource, batchTask); 
            if (results != null)
                exceptions.addAll(results);
            
            progress.setPercentage(100);
            if (!exceptions.isEmpty())
            {
                progress.setCompletedSuccessfully(false);
                progress.setStatus("Conversion completed with " + exceptions.size() + " exceptions.");
            }
            else
            {
                progress.setCompletedSuccessfully(true);
                progress.setStatus("Conversion completed with no exceptions.");
            }
        }
        catch (Exception ex)
        {
            exceptions.add(ex);
            progress.setCompletedSuccessfully(false);
            progress.setPercentage(100);
            StringWriter exOutput = new StringWriter();
            PrintWriter writer = new PrintWriter(exOutput);
            ex.printStackTrace(writer);
            
            writer.flush();
            String exStr = "<pre>" + exOutput.getBuffer().toString() + "</pre>";
            exStr.replaceAll("\n", "<br>");
            
            progress.setStatus("<b>Error</b>:<br>" + exStr);
        }
        finally
        {
            runner.shutdown();
        }
    }

    public List<Exception> getExceptions()
    {
        return exceptions;
    }
}
