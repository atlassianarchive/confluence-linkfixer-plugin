package com.atlassian.confluence.plugins.linkfixer;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import net.sf.hibernate.Criteria;
import net.sf.hibernate.HibernateException;
import net.sf.hibernate.Query;
import net.sf.hibernate.Session;
import net.sf.hibernate.transform.ResultTransformer;

import com.atlassian.confluence.core.SpaceContentEntityObject;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.hibernate.PluginHibernateSessionFactory;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;

public class HibernatePageLinkQueryDao implements PageLinkQueryDao
{
    private static final String CONTENT_WHICH_LINK_TO_SPACE_FROM_PART =
            "from com.atlassian.confluence.core.SpaceContentEntityObject as sceo " +
            "inner join sceo.outgoingLinks links " +
            "where sceo.space = :space " +
            "and sceo.contentStatus = 'current' " +
            "and links.destinationSpaceKey = :spaceKey ";

    private static final String AND_ID_GREATER_PART = "and sceo.id >= :startContentId ";
    
    private static final String ORDERBY_ID_PART = "order by sceo.id";

    private static final String IDS_WHICH_LINK_TO_SPACE =
            "select sceo.id, sceo.title " + CONTENT_WHICH_LINK_TO_SPACE_FROM_PART + ORDERBY_ID_PART;

    private static final String CONTENT_WHICH_LINK_TO_SPACE = 
            "select sceo " + CONTENT_WHICH_LINK_TO_SPACE_FROM_PART + AND_ID_GREATER_PART + ORDERBY_ID_PART;
    
    private static final String COUNT_CONTENT_WHICH_LINK_TO_SPACE =
            "select count(sceo.id) " + CONTENT_WHICH_LINK_TO_SPACE_FROM_PART;    
    
    private PluginHibernateSessionFactory sessionFactory;
    private TransactionTemplate transactionTemplate;
    
    public HibernatePageLinkQueryDao(PluginHibernateSessionFactory pluginHibernateSessionFactory, TransactionTemplate transactionTemplate)
    {
        this.sessionFactory = pluginHibernateSessionFactory;
        this.transactionTemplate = transactionTemplate;
    }

    @SuppressWarnings("unchecked")
    public List<PageIdentifyingReportItem> getReportOfSpacedContentWhichLinkToSpace(final Space space, final Space destinationSpace)
    {
        return transactionTemplate.execute(new TransactionCallback<List<PageIdentifyingReportItem>>()
        {
            @Override
            public List<PageIdentifyingReportItem> doInTransaction()
            {
                Session session = sessionFactory.getSession();
                try
                {
                    Set<PageIdentifyingReportItem> result = new HashSet<PageIdentifyingReportItem>();
                    
                    Query getContentInSpace;
                    getContentInSpace = session.createQuery(IDS_WHICH_LINK_TO_SPACE);
                    getContentInSpace.setParameter("space", space);
                    getContentInSpace.setParameter("spaceKey", destinationSpace.getKey());
                    
                    Iterator it = getContentInSpace.iterate();
                    while (it.hasNext())
                    {
                        Object[] row = (Object[])it.next();
                        
                        Long id = (Long)row[0];
                        String title = (String)row[1];
                        
                        result.add(new PageIdentifyingReportItem(id, title));
                    }
            
                    List<PageIdentifyingReportItem> listResult = new ArrayList<PageIdentifyingReportItem>(result);            
                    return listResult;        
                }
                catch (HibernateException ex)
                {
                    throw new RuntimeException(ex);
                }
            }
        });
    }

    @Override
    public List<SpaceContentEntityObject> getSpacedContentWhichLinkToSpace(final Space space, final Space destinationSpace,
            final int maxRows)
    {
        return getSpacedContentWhichLinkToSpace(space, destinationSpace, 0, maxRows);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<SpaceContentEntityObject> getSpacedContentWhichLinkToSpace(final Space space, final Space destinationSpace,
            final long startContentId, final int maxRows)
    {
        return transactionTemplate.execute(new TransactionCallback<List<SpaceContentEntityObject>>()
        {
            @Override
            public List<SpaceContentEntityObject> doInTransaction()
            {
                Session session = sessionFactory.getSession();

                try
                {
                    Query getContentInSpace = session.createQuery(CONTENT_WHICH_LINK_TO_SPACE);
                    getContentInSpace.setParameter("space", space);
                    getContentInSpace.setParameter("spaceKey", destinationSpace.getKey());
                    getContentInSpace.setParameter("startContentId", startContentId);
                    getContentInSpace.setMaxResults(maxRows);

                    List results = getContentInSpace.list();
                    final ResultTransformer trans = Criteria.DISTINCT_ROOT_ENTITY;
                    return trans.transformList(results);
                }
                catch (HibernateException ex)
                {
                    throw new RuntimeException(ex);
                }                
            }
        });
    }

    @Override
    public int countSpacedContentWhichLinkToSpace(final Space space, final Space destinationSpace)
    {
        return transactionTemplate.execute(new TransactionCallback<Integer>()
        {
            @Override
            public Integer doInTransaction()
            {
                Session session = sessionFactory.getSession();

                try
                {
                    Query getContentInSpace = session.createQuery(COUNT_CONTENT_WHICH_LINK_TO_SPACE);
                    getContentInSpace.setParameter("space", space);
                    getContentInSpace.setParameter("spaceKey", destinationSpace.getKey());

                    Integer result = (Integer) getContentInSpace.uniqueResult();
                    return result;
                }
                catch (HibernateException ex)
                {
                    throw new RuntimeException(ex);
                }                
            }
        });
    }   
}
