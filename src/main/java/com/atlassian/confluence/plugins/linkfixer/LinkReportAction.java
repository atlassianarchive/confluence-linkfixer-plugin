package com.atlassian.confluence.plugins.linkfixer;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;

/**
 * An action that allows the triggering of a report about links within a specific space.
 */
public class LinkReportAction extends ConfluenceActionSupport
{
    private SpaceManager spaceManager;
    private PageLinkQueryDao pageLinkQueryDao;
    
    private String spaceKey;
    private String destinationSpaceKey;
    
    // Action results
    private Space space;
    private Space destinationSpace;
    private List<PageIdentifyingReportItem> report;
    
    
    @Override
    public void validate()
    {
        if (spaceKey.equals(destinationSpaceKey))
            addActionError("Checking for absolute links which should be relative within a space is not supported. Use the 'Links which should be relative report'.");
        
        if (StringUtils.isBlank(spaceKey) || StringUtils.isBlank(destinationSpaceKey))
            addActionError("Both a space key and destination space key are required.");
        
        space = spaceManager.getSpace(spaceKey);
        if (space == null)
            addActionError("error.space.could.not.be.found", spaceKey);

        destinationSpace = spaceManager.getSpace(destinationSpaceKey);
        if (destinationSpace == null)
            addActionError("error.space.could.not.be.found", destinationSpaceKey);
    }

    /**
     * Run the report for the identified space.
     * 
     * @return
     */
    public String doReport()
    {
        report = pageLinkQueryDao.getReportOfSpacedContentWhichLinkToSpace(space, destinationSpace);
        return SUCCESS;
    }
    
    public void setPageLinkQueryDao(PageLinkQueryDao pageLinkQueryDao)
    {
        this.pageLinkQueryDao = pageLinkQueryDao;
    }
    
    public void setSpaceManager(SpaceManager spaceManager)
    {
        this.spaceManager = spaceManager;
    }

    public String getSpaceKey()
    {
        return spaceKey;
    }

    public void setSpaceKey(String spaceKey)
    {
        this.spaceKey = spaceKey;
    }

    public String getDestinationSpaceKey()
    {
        return destinationSpaceKey;
    }

    public void setDestinationSpaceKey(String destinationSpaceKey)
    {
        this.destinationSpaceKey = destinationSpaceKey;
    }

    public List<PageIdentifyingReportItem> getReport()
    {
        return report;
    }
}
