package com.atlassian.confluence.plugins.linkfixer;

import com.atlassian.confluence.content.render.xhtml.migration.BatchException;
import com.atlassian.confluence.content.render.xhtml.migration.BatchTask;
import com.atlassian.confluence.core.ContentEntityManager;
import com.atlassian.confluence.core.DefaultSaveContext;
import com.atlassian.confluence.core.Modification;
import com.atlassian.confluence.core.SaveContext;
import com.atlassian.confluence.core.SpaceContentEntityObject;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.user.User;

/**
 * A BatchTask that will convert absolute links to a particular space into relative links
 * to the contents own space.
 */
public class CreateRelativeLinksBatchTask implements BatchTask<SpaceContentEntityObject>
{
    private static final SaveContext saveContext = new DefaultSaveContext(true, true, true);

    private final String spaceKey;
    private final ResourceIdentifierModifier resourceIdentifierModifier;
    private final ContentEntityManager contentEntityManager;
    private final User user;
    
    public CreateRelativeLinksBatchTask(String spaceKey, ResourceIdentifierModifier resourceIdentifierModifier,
            ContentEntityManager contentEntityManager, User user)
    {
        this.spaceKey = spaceKey;
        this.resourceIdentifierModifier = resourceIdentifierModifier;
        this.contentEntityManager = contentEntityManager;
        this.user = user;
    }

    @Override
    public void apply(SpaceContentEntityObject item, int index, int batchSize) throws Exception, BatchException
    {
        AuthenticatedUserThreadLocal.setUser(user); // so the modification has a user
        String content = item.getBodyAsString();
        
        final ResourceIdentifierModifierResult result = resourceIdentifierModifier.removeSpaceKey(content, spaceKey);

        if (result.isModified())
        {
            contentEntityManager.saveNewVersion(item, new Modification<SpaceContentEntityObject>() {

                @Override
                public void modify(SpaceContentEntityObject content)
                {
                    content.setVersionComment("[Link Fixer plugin] Converted all links to the '" + spaceKey + "' space to be relative links instead.");
                    content.setBodyAsString(result.getContent());
                }
                
            }, saveContext);
        }
    }
}
