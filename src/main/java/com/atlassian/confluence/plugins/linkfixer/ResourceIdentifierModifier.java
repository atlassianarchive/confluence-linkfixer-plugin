package com.atlassian.confluence.plugins.linkfixer;

public interface ResourceIdentifierModifier
{
    /**
     * Convert any elements with ri:space-key attributes with oldKey to have the value newKey.
     * 
     * @param content the content to be modified
     * @param oldKey the value of the ri:space-key attribute to be changed
     * @param newKey the new value to use for the ri:space-key attribute
     * 
     * @return the supplied content modified
     */
//    public String convertSpaceKey(String content, String oldKey, String newKey);
    
    /**
     * Remove any ri:space-key attributes from an element when its value matches the keyValue
     * supplied.
     * 
     * @param content the content to be modified
     * @param keyValue the value of ri:space-key attributes to remove
     * @return the supplied content with any matching ri:space-key attributes removed.
     */
    public ResourceIdentifierModifierResult removeSpaceKey(String content, String keyValue);
    
    /**
     * @param content
     * @param key
     * @return a count of how many resource identifiers are found that reference the identified space.
     */
    public int countSpaceKeyReferences(String content, String key);
}
