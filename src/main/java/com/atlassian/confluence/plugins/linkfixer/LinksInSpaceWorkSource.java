package com.atlassian.confluence.plugins.linkfixer;

import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.atlassian.confluence.content.render.xhtml.migration.BatchableWorkSource;
import com.atlassian.confluence.core.SpaceContentEntityObject;
import com.atlassian.confluence.spaces.Space;

/**
 * Source that provides batches of {@link com.atlassian.confluence.core.ContentEntityObject}s that require migration.
 * <p>
 * This class is thread-safe.   
 */
public class LinksInSpaceWorkSource implements BatchableWorkSource<SpaceContentEntityObject>
{
    private final PageLinkQueryDao pageLinkQueryDao;
    private final int batchSize;    
    private final Space space;
    private final Space destinationSpace;
    
    private int numberOfBatches = -1;
    private int batchesRetrieved = 0;
    private long startContentId = -1;

    public LinksInSpaceWorkSource(PageLinkQueryDao pageLinkQueryDao, Space space, Space destinationSpace, int batchSize)
    {
        this.pageLinkQueryDao = pageLinkQueryDao;
        this.space = space;
        this.destinationSpace = destinationSpace;
        this.batchSize = batchSize;
    }

    /**
     * Returns a batch of work. When work is exhausted an empty list is returned.
     *
     * @return batch of work
     */
    public synchronized List<SpaceContentEntityObject> getBatch()
    {
        if (!hasMoreBatches())
            return Collections.emptyList();

        List<SpaceContentEntityObject> result;

        if (startContentId == -1)
        {
            result = pageLinkQueryDao.getSpacedContentWhichLinkToSpace(space, destinationSpace, batchSize);
        }
        else
        {
            result = pageLinkQueryDao.getSpacedContentWhichLinkToSpace(space, destinationSpace, startContentId, batchSize);
        }

//        System.out.println("Returning " + result.size() + " results in batch " + (batchesRetrieved + 1) + " starting from content Id " + (startContentId == -1 ? 0 : startContentId));
        
        batchesRetrieved++;
        updateStartContentIdForNextBatch(result);
        return result;
    }

    /**
     * Because the results are ordered by content id we know the last item in the list will be
     * the largest content id so ensure that the next query only returns content above this id.
     */
    private void updateStartContentIdForNextBatch(List<SpaceContentEntityObject> contentIds)
    {
        if (CollectionUtils.isEmpty(contentIds))
            return;
        
        SpaceContentEntityObject lastResult = contentIds.get(contentIds.size() - 1);
        startContentId = lastResult.getId() + 1;
    }

    public synchronized boolean hasMoreBatches()
    {
        return batchesRetrieved < numberOfBatches();
    }

    public synchronized int numberOfBatches()
    {
        if (numberOfBatches == -1)
        {
            int batches = pageLinkQueryDao.countSpacedContentWhichLinkToSpace(space, destinationSpace);
            
            numberOfBatches = (batches / batchSize) + (batches % batchSize > 0 ? 1 : 0);
//            System.out.println("There are " + numberOfBatches + " batches of work.");
        }

        return numberOfBatches;
    }
}