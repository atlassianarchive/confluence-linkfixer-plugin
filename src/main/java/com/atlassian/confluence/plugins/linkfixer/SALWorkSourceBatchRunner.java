package com.atlassian.confluence.plugins.linkfixer;

import static com.atlassian.util.concurrent.ThreadFactories.namedThreadFactory;
import static com.atlassian.util.concurrent.ThreadFactories.Type.DAEMON;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;

import org.apache.commons.lang.StringUtils;

import com.atlassian.confluence.content.render.xhtml.migration.BatchException;
import com.atlassian.confluence.content.render.xhtml.migration.BatchTask;
import com.atlassian.confluence.content.render.xhtml.migration.BatchableWorkSource;
import com.atlassian.core.util.ProgressMeter;
import com.atlassian.core.util.ProgressWrapper;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.user.User;


public class SALWorkSourceBatchRunner<T>
{
    private TransactionTemplate transactionTemplate;
    
    private final ExecutorService executor;
    
    // optional 
    private ProgressMeter progress;

    public SALWorkSourceBatchRunner(final String threadName, int numThreads,
            TransactionTemplate transactionTemplate)
    {
        if (StringUtils.isBlank(threadName))
            throw new IllegalArgumentException("A threadName is required.");

        if (numThreads <= 0)
            throw new IllegalArgumentException("1 or more threads are required.");

        this.transactionTemplate = transactionTemplate;
        this.executor = Executors.newFixedThreadPool(numThreads, namedThreadFactory(threadName, DAEMON));
    }

    /**
     * Begin executing against all the work in the supplied work source. One or more threads will be spawned to perform
     * the work but this method will wait until all threads have finished before it returns.
     * 
     * @param workSource
     * @param task the task to be run on each item in the batches from the work source
     * @return a list of any individual exceptions that occurred as the WorkSourceTask was executing. An empty list will
     *         be returned if there are no exceptions.
     * @throws ExecutionException 
     * @throws InterruptedException 
     * @throws Exception if there is any problem out with the actual execution of the the BatchTasks. (Problems within the
     * BatchTask are returned in the List of Exceptions.)
     */
    public List<Exception> run(final BatchableWorkSource<T> workSource, final BatchTask<T> task) throws ExecutionException, InterruptedException
    {
        List<Future<List<Exception>>> futures = new ArrayList<Future<List<Exception>>>(workSource.numberOfBatches());
        final ProgressWrapper wrapper = progress != null ? new ProgressWrapper(progress, workSource.numberOfBatches()) : null;
        for (int i = 0; i < workSource.numberOfBatches(); i++)
        {
            futures.add(executor.submit(new Callable<List<Exception>>() {
                @Override
                public List<Exception> call() throws Exception
                {
                    final List<Exception> exceptions = new ArrayList<Exception>(1);
                    
                    // wrap a transaction around each batch
                    transactionTemplate.execute(new TransactionCallback<Object>()
                    {
                        @Override
                        public Object doInTransaction()
                        {
                            List<T> batch = workSource.getBatch();
                            try
                            {
                                for (int i = 0, size = batch.size(); i < size; i++)
                                {
                                    try
                                    {
                                        task.apply(batch.get(i), i, batch.size());
                                    }
                                    catch(BatchException ex)
                                    {
                                        exceptions.addAll(ex.getBatchExceptions());
                                    }
                                    catch (Exception ex) // do not let exceptions propagate out and cause a rollback of the batch
                                    {
                                        exceptions.add(ex);
                                    }           
                                }
                            }
                            finally
                            {
                                if(wrapper != null)
                                    wrapper.incrementCounter();
                            }
                            
                            return null;
                        }
                        
                    });
                    return exceptions;
                }
            }));
        }
            
        List<Exception> allExceptions = new ArrayList<Exception>(1);
        for (Future<List<Exception>> future : futures)
        {
            allExceptions.addAll(future.get()); // wait for all batches to complete
        }
        if(wrapper != null)
            wrapper.setStatus("Finished");
                
        
        return allExceptions;
    }

    public void shutdown()
    {
        executor.shutdown();
    }
    
    public void setProgressWrapper(ProgressMeter progress)
    {
        this.progress = progress;
    }
    
    private static final class UserSetThreadFactory implements ThreadFactory
    {
        private final User user;

        public UserSetThreadFactory(User user)
        {
            this.user = user;
        }

        @Override
        public Thread newThread(Runnable arg0)
        {
            // TODO Auto-generated method stub
            return null;
        }
        
        
    }
}
