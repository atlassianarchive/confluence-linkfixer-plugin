package com.atlassian.confluence.plugins.linkfixer.ownspace;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.plugins.linkfixer.PageIdentifyingReportItem;
import com.atlassian.confluence.plugins.linkfixer.PageLinkQueryDao;
import com.atlassian.confluence.plugins.linkfixer.ResourceIdentifierModifier;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.util.longrunning.LongRunningTaskId;
import com.atlassian.confluence.util.longrunning.LongRunningTaskManager;
import com.atlassian.core.task.longrunning.LongRunningTask;
import com.atlassian.sal.api.transaction.TransactionTemplate;

/**
 * An action that triggers a long running task to search content for absolute references
 * that should be relative
 */
public class AbsoluteLinkReportAction extends ConfluenceActionSupport
{
    private SpaceManager spaceManager;
    private PageLinkQueryDao pageLinkQueryDao;
    private ResourceIdentifierModifier resourceIdentifierModifier;
    private LongRunningTaskManager longRunningTaskManager;
    private TransactionTemplate transactionTemplate;
    
    private String spaceKey;
    
    private LongRunningTaskId taskId;
    private LongRunningTask task;
    
    // Action results
    private Space space;
    private List<PageIdentifyingReportItem> report;
    private int processedCount;
    
    
    @Override
    public void validate()
    {
        if (StringUtils.isBlank(spaceKey))
            addActionError("A space key is required.");
        
        space = spaceManager.getSpace(spaceKey);
        if (space == null)
            addActionError("The space with key '" + spaceKey + "' could not be found.");            
    }

    /**
     * Instigate the long running conversion process.
     * 
     * @return
     */
    public String doReport()
    {
        task = new AbsoluteLinkReportLongRunningTask(pageLinkQueryDao, space, transactionTemplate, resourceIdentifierModifier);
        
        taskId = longRunningTaskManager.startLongRunningTask(getRemoteUser(), task);
        return SUCCESS;
    }
    
    public String doShowReport()
    {
        AbsoluteLinkReportLongRunningTask longRunningTask = (AbsoluteLinkReportLongRunningTask) getTask();
        if (longRunningTask == null)
        {
            report = Collections.emptyList();
            processedCount = 0;
        }
        else
        {
            AbsoluteLinkReport taskReport = longRunningTask.getReport();
            report = new ArrayList<PageIdentifyingReportItem>(taskReport.getEntries());     
            processedCount = taskReport.getProcessed();
        }
        
        return SUCCESS;
    }
    
    public void setPageLinkQueryDao(PageLinkQueryDao pageLinkQueryDao)
    {
        this.pageLinkQueryDao = pageLinkQueryDao;
    }
    
    public void setSpaceManager(SpaceManager spaceManager)
    {
        this.spaceManager = spaceManager;
    }

    public void setTransactionTemplate(TransactionTemplate transactionTemplate)
    {
        this.transactionTemplate = transactionTemplate;
    }

    public void setResourceIdentifierModifier(ResourceIdentifierModifier resourceIdentifierModifier)
    {
        this.resourceIdentifierModifier = resourceIdentifierModifier;
    }

    public void setLongRunningTaskManager(LongRunningTaskManager longRunningTaskManager)
    {
        this.longRunningTaskManager = longRunningTaskManager;
    }

    public String getSpaceKey()
    {
        return spaceKey;
    }

    public void setSpaceKey(String spaceKey)
    {
        this.spaceKey = spaceKey;
    }

    public String getTaskId()
    {
        return taskId.toString();
    }
    
    public void setTaskId(String taskId)
    {
        this.taskId = LongRunningTaskId.valueOf(taskId);
    }

    public LongRunningTask getTask()
    {
        if (task == null && taskId != null)
        {
            return longRunningTaskManager.getLongRunningTask(getRemoteUser(), taskId);
        }
        else
        {
            return null;
        }
    }

    public List<PageIdentifyingReportItem> getReport()
    {
        return report;
    }

    public int getProcessedCount()
    {
        return processedCount;
    }
}
