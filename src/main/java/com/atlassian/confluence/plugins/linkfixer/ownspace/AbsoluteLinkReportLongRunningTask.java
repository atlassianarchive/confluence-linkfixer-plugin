package com.atlassian.confluence.plugins.linkfixer.ownspace;

import java.io.PrintWriter;
import java.io.StringWriter;

import com.atlassian.confluence.content.render.xhtml.migration.BatchTask;
import com.atlassian.confluence.content.render.xhtml.migration.BatchableWorkSource;
import com.atlassian.confluence.core.SpaceContentEntityObject;
import com.atlassian.confluence.plugins.linkfixer.LinksInSpaceWorkSource;
import com.atlassian.confluence.plugins.linkfixer.PageLinkQueryDao;
import com.atlassian.confluence.plugins.linkfixer.ResourceIdentifierModifier;
import com.atlassian.confluence.plugins.linkfixer.SALWorkSourceBatchRunner;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.util.longrunning.ConfluenceAbstractLongRunningTask;
import com.atlassian.sal.api.transaction.TransactionTemplate;

public class AbsoluteLinkReportLongRunningTask extends ConfluenceAbstractLongRunningTask
{
    private final BatchableWorkSource<SpaceContentEntityObject> workSource;    
    private final SALWorkSourceBatchRunner<SpaceContentEntityObject> runner;
    private final BatchTask<SpaceContentEntityObject> batchTask;

    // Task result
    private AbsoluteLinkReport report;
    
    public AbsoluteLinkReportLongRunningTask(PageLinkQueryDao pageLinkQueryDao, Space space,
            TransactionTemplate transactionTemplate, ResourceIdentifierModifier resourceIdentifierModifier)
    {
        super();
        this.workSource = new LinksInSpaceWorkSource(pageLinkQueryDao, space, space, Integer.getInteger("upgrade.fix.batch.size", 250));
        this.runner = new SALWorkSourceBatchRunner<SpaceContentEntityObject>("Check for absolute links thread", Integer.getInteger("upgrade.fix.threads", 4), transactionTemplate);
        this.runner.setProgressWrapper(progress);
        this.report = new AbsoluteLinkReport();
        this.batchTask = new AbsoluteLinksReportingBatchTask(space.getKey(), resourceIdentifierModifier, report);
    }
    
    @Override
    public String getName()
    {
        return "Search for incorrect absolute links";
    }

    @Override
    protected void runInternal()
    {
        try
        {
            runner.run(workSource, batchTask); 
            progress.setPercentage(100);
            progress.setCompletedSuccessfully(true);
            progress.setStatus("Report complete.");
        }
        catch (Exception ex)
        {
            progress.setCompletedSuccessfully(false);
            progress.setPercentage(100);
            StringWriter exOutput = new StringWriter();
            PrintWriter writer = new PrintWriter(exOutput);
            ex.printStackTrace(writer);
            
            writer.flush();
            String exStr = "<pre>" + exOutput.getBuffer().toString() + "</pre>";
            exStr.replaceAll("\n", "<br>");
            
            progress.setStatus("<b>Error</b>:<br>" + exStr);
        }
        finally
        {
            runner.shutdown();
        }
    }

    public AbsoluteLinkReport getReport()
    {
        return report;
    }
}
