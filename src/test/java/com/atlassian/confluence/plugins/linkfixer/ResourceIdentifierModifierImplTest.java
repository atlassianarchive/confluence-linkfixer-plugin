package com.atlassian.confluence.plugins.linkfixer;

import java.io.IOException;
import java.util.List;

import javax.xml.stream.XMLStreamException;

import org.custommonkey.xmlunit.XMLTestCase;
import org.xml.sax.SAXException;

import com.atlassian.confluence.content.render.xhtml.DefaultXmlEventReaderFactory;
import com.atlassian.confluence.content.render.xhtml.Namespace;
import com.atlassian.confluence.content.render.xhtml.XhtmlConstants;

public class ResourceIdentifierModifierImplTest extends XMLTestCase
{
    private ResourceIdentifierModifier modifier;
    
    protected void setUp() throws Exception
    {
        super.setUp();

        modifier = new ResourceIdentifierModifierImpl(new DefaultXmlEventReaderFactory());
    }
    
    public void testNoModification() throws Exception
    {
        String input = "<p><b>apples</b><ri:resource ri:space-key=\"WRONG\"/><em>pears</em></p>";
        
        String modified = modifier.removeSpaceKey(input, "RIGHT").getContent();
        
        assertStorageXmlEquals(input, modified);
    }    

    public void testNoModificationForNonNamespacedSpaceKey() throws Exception
    {
        String input = "<p><b>apples</b><ri:resource space-key=\"ABC\"/><em>pears</em></p>";

        String modified = modifier.removeSpaceKey(input, "ABC").getContent();
        
        assertStorageXmlEquals(input, modified);
    }
    
    public void testModifyMultiple() throws Exception
    {
        String input = "<p><b>apples</b><ri:resource ri:a=\"b\" ri:space-key=\"ABC\" c=\"d\"/><em>pears</em><ri:another ri:space-key=\"ABC\"/><i>italic</i><ri:another ri:space-key=\"DEF\"/></p>";
        String expected = "<p><b>apples</b><ri:resource ri:a=\"b\" c=\"d\"/><em>pears</em><ri:another/><i>italic</i><ri:another ri:space-key=\"DEF\"/></p>";

        String modified = modifier.removeSpaceKey(input, "ABC").getContent();
        
        assertStorageXmlEquals(expected, modified);        
    }
    
    public void testEntitiesPreserved() throws Exception
    {
        String input = "<p>&lt; and of course &gt;</p><p>Second &amp;</p>";
        String modified = modifier.removeSpaceKey(input, "ABC").getContent();
        
        assertStorageXmlEquals(input, modified);
    }
    
    public void testNonEntitiesPreserved() throws Exception
    {
        String input = "<p>a  b 'c'</p>";
        String modified = modifier.removeSpaceKey(input, "ABC").getContent();
        
        assertStorageXmlEquals(input, modified);
    }
    
    public void testSpaceResourceIdentifiersNotModified() throws Exception
    {
        String input = "<p>abcdef</p><p><ri:space ri:space-key=\"ABC\" /> ghijk</p>";
        String modified = modifier.removeSpaceKey(input, "ABC").getContent();
        
        assertStorageXmlEquals(input, modified);
    }
    
    public void assertStorageXmlEquals(String expected, String actual) throws XMLStreamException, SAXException, IOException
    {
        String nsExpected = createStorageFormatXml(expected);
        String nsActual = createStorageFormatXml(actual);
        assertXMLEqual(nsExpected, nsActual);
    }
    
    public String createStorageFormatXml(String xml)
    {
        return createXmlWithNamespaces(xml, XhtmlConstants.STORAGE_NAMESPACES);
    }

    public String createXmlWithNamespaces(String xml, List<Namespace> namespaces)
    {
        StringBuilder storageXml = new StringBuilder();
        storageXml.append("<xml");

        for (Namespace namespace : namespaces)
            storageXml.append(" xmlns" + (namespace.isDefaultNamespace() ? "" : ":" + namespace.getPrefix()) + "=\"" + namespace.getUri() + "\"");

        storageXml.append(">").append(xml).append("</xml>");
        return storageXml.toString();
    }    
}